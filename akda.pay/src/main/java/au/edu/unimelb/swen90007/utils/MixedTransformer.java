package au.edu.unimelb.swen90007.utils;

import flexjson.TransformerUtil;
import flexjson.transformer.AbstractTransformer;
import flexjson.transformer.TypeTransformerMap;

/**
 * A general purpose transformer which uses {@link TransformerUtil#getDefaultTypeTransformers} to
 * delegate transformers for an object.
 */
public class MixedTransformer extends AbstractTransformer {

  private TypeTransformerMap typeTransformerMap = new TypeTransformerMap(
      TransformerUtil.getDefaultTypeTransformers());

  @Override
  public void transform(Object object) {
    typeTransformerMap.getTransformer(object).transform(object);
  }

}
