package au.edu.unimelb.swen90007.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import au.edu.unimelb.swen90007.utils.HttpServletRequestUtils;

/**
 * Authentication failure handler that can discriminate AJAX requests.
 */
public class AjaxEnabledAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
  private final Logger log = Logger.getLogger(this.getClass());

  /**
   * If request is AJAX, returns JSON format indicating login failure. Otherwise delegates to parent
   * @link{SimpleUrlAuthenticationFailureHandler}
   * 
   * @param request HTTP request
   * @param response HTTP response
   * @param exception authorisation exception that caused failure
   * @throws IOException
   * @throws ServletException
   */
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException exception) throws IOException, ServletException {
    if (HttpServletRequestUtils.isAjaxRequest(request)) {
      log.info("AJAX Authentication Failure");
      response.getWriter().print(
          "{\"success\":false, \"errors\": { \"reason\": \"" + exception.getMessage() + "\" }}");
      response.getWriter().flush();
    } else {
      log.info("Non-AJAX Authentication Failure");
      super.onAuthenticationFailure(request, response, exception);
    }
  }
}
