package au.edu.unimelb.swen90007.transformer;

import org.joda.time.DateTime;

import flexjson.JSONSerializer;

public class EntitySerializer extends JSONSerializer {

	public EntitySerializer() {
		this.exclude("*.class");
		this.transform(new DateTimeObjectFactory(), DateTime.class);
	}
	
}
