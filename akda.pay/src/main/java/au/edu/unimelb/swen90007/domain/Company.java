package au.edu.unimelb.swen90007.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.plural.RooPlural;

import flexjson.JSONSerializer;

@RooJavaBean
@RooJpaActiveRecord
@RooJson(deepSerialize = true)
@RooPlural(value = "Companies")
public class Company {
	
	
//	private static JSONSerializer serializer = new JSONSerializer().include("name").exclude("*");

	@NotNull
	@Size(max = 50)
	private String name;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeRole> employees = new HashSet<EmployeeRole>();

	public String toJson() {
		JSONSerializer serializer = new JSONSerializer().include("name", "id").exclude("*");
        return serializer.deepSerialize(this);
    }
	
	public static String toJsonArray(Collection<Company> collection) {
		JSONSerializer serializer = new JSONSerializer().include("name", "id").exclude("*");
        return serializer.deepSerialize(collection);
    }
	
}
