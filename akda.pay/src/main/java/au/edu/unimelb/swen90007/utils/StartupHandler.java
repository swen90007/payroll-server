package au.edu.unimelb.swen90007.utils;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import au.edu.unimelb.swen90007.domain.AdminRole;
import au.edu.unimelb.swen90007.domain.BreakEvent;
import au.edu.unimelb.swen90007.domain.Company;
import au.edu.unimelb.swen90007.domain.EmployeeRole;
import au.edu.unimelb.swen90007.domain.HRManagerRole;
import au.edu.unimelb.swen90007.domain.PayrollManagerRole;
import au.edu.unimelb.swen90007.domain.TimeSheet;
import au.edu.unimelb.swen90007.domain.UserAccount;
import au.edu.unimelb.swen90007.domain.WorkEvent;

/**
 * A class which is initialised at startup to perform various tasks.
 *
 */
public class StartupHandler {

	private final Logger log = Logger.getLogger(this.getClass());
	
	/**
	 * Creates dummy data in the server.
	 */
	@PostConstruct
	@Transactional
	public void setUpInitialData() {
		if (UserAccount.findAllUserAccounts().size() == 0) {
			log.info("Initialising startup data!");
			UserAccount user1 = new UserAccount();
			user1.setUsername("user");
			user1.setPassword("pass");
			user1.setName("John Smith");
			user1.persist();
			
			UserAccount user2 = new UserAccount();
			user2.setUsername("david");
			user2.setPassword("whitey");
			user2.setName("David White");
			user2.persist();
			
			Company company1 = new Company();
			company1.setName("ACME");
			company1.persist();
			
			EmployeeRole role1 = new EmployeeRole();
			role1.setUser(user2);
			role1.setCompany(company1);
			role1.setTaxFileNumber(374273948);
			role1.setHourlyWageRate(30);
			TimeSheet timesheet1 = new TimeSheet();
			role1.setTimesheet(timesheet1);
			role1.persist();
			
			WorkEvent event1 = new WorkEvent();
			event1.setStartTime(new DateTime(2013, 10, 23, 10, 0, 0));
			event1.setEndTime(new DateTime(2013, 10, 23, 13, 0, 0));
			event1.setDescription("Worked on the Johnson portfolio for Monday.");
			event1.setTimesheet(timesheet1);
			event1.persist();
			
			BreakEvent event2 = new BreakEvent();
			event2.setStartTime(new DateTime(2013, 10, 23, 13, 0, 0));
			event2.setEndTime(new DateTime(2013, 10, 23, 14, 0, 0));
			event2.setTimesheet(timesheet1);
			event2.persist();
			
			WorkEvent event3 = new WorkEvent();
			event3.setStartTime(new DateTime(2013, 10, 23, 14, 0, 0));
			event3.setEndTime(new DateTime(2013, 10, 23, 17, 0, 0));
			event3.setDescription("Finshed the presentation for Monday.");
			event3.setTimesheet(timesheet1);
			event3.persist();
			
			HRManagerRole role2 = new HRManagerRole();
			role2.setUser(user1);
			role2.setCompany(company1);
			role2.setTaxFileNumber(544274948);
			role2.setHourlyWageRate(40);
			TimeSheet timesheet2 = new TimeSheet();
			role2.setTimesheet(timesheet2);
			role2.persist();
			
			WorkEvent event4 = new WorkEvent();
			event4.setStartTime(new DateTime(2013, 10, 23, 8, 0, 0));
			event4.setEndTime(new DateTime(2013, 10, 23, 20, 0, 0));
			event4.setDescription("Lots of work done.");
			event4.setTimesheet(timesheet2);
			event4.persist();
			
			WorkEvent event5 = new WorkEvent();
			event5.setStartTime(new DateTime(2013, 10, 24, 12, 0, 0));
			event5.setEndTime(new DateTime(2013, 10, 24, 18, 0, 0));
			event5.setDescription("Lots more of work done.");
			event5.setTimesheet(timesheet2);
			event5.persist();
			
			BreakEvent event6 = new BreakEvent();
			event6.setStartTime(new DateTime(2013, 10, 24, 18, 0, 0));
			event6.setEndTime(new DateTime(2013, 10, 24, 22, 0, 0));
			event6.setDescription("Took a break.");
			event6.setTimesheet(timesheet2);
			event6.persist();
			
			PayrollManagerRole role3 = new PayrollManagerRole();
			role3.setUser(user1);
			role3.setCompany(company1);
			role3.setTaxFileNumber(343576938);
			role3.setHourlyWageRate(45);
			TimeSheet timesheet3 = new TimeSheet();
			role3.setTimesheet(timesheet3);
			role3.persist();
			
			WorkEvent event7 = new WorkEvent();
			event7.setStartTime(new DateTime(2013, 10, 26, 8, 0, 0));
			event7.setEndTime(new DateTime(2013, 10, 26, 12, 0, 0));
			event7.setDescription("Lots of work done.");
			event7.setTimesheet(timesheet3);
			event7.persist();
			
			WorkEvent event8 = new WorkEvent();
			event8.setStartTime(new DateTime(2013, 10, 26, 12, 0, 0));
			event8.setEndTime(new DateTime(2013, 10, 26, 14, 0, 0));
			event8.setDescription("Did some work again.");
			event8.setTimesheet(timesheet3);
			event8.persist();
			
			BreakEvent event9 = new BreakEvent();
			event9.setStartTime(new DateTime(2013, 10, 26, 14, 0, 0));
			event9.setEndTime(new DateTime(2013, 10, 26, 15, 0, 0));
			event9.setDescription("Took a short break.");
			event9.setTimesheet(timesheet3);
			event9.persist();
			
			Company company2 = new Company();
			company2.setName("GLOBADEX");
			company2.persist();
			
			EmployeeRole role4 = new EmployeeRole();
			role4.setUser(user2);
			role4.setCompany(company2);
			role4.setTaxFileNumber(743675948);
			role4.setHourlyWageRate(35);
			TimeSheet timesheet4 = new TimeSheet();
			role4.setTimesheet(timesheet4);
			role4.persist();
			
			WorkEvent event10 = new WorkEvent();
			event10.setStartTime(new DateTime(2013, 10, 27, 10, 0, 0));
			event10.setEndTime(new DateTime(2013, 10, 27, 15, 0, 0));
			event10.setDescription("A few remaining docs written.");
			event10.setTimesheet(timesheet4);
			event10.persist();
			
			WorkEvent event11 = new WorkEvent();
			event11.setStartTime(new DateTime(2013, 10, 27, 16, 0, 0));
			event11.setEndTime(new DateTime(2013, 10, 27, 18, 0, 0));
			event11.setDescription("All docs complete.");
			event11.setTimesheet(timesheet4);
			event11.persist();
			
			BreakEvent event12 = new BreakEvent();
			event12.setStartTime(new DateTime(2013, 10, 27, 15, 0, 0));
			event12.setEndTime(new DateTime(2013, 10, 27, 16, 0, 0));
			event12.setDescription("Short break.");
			event12.setTimesheet(timesheet4);
			
			AdminRole role5 = new AdminRole();
			role5.setUser(user1);
			role5.persist();
			
			log.info("Company has employees: " + company1.getEmployees().size());
		}
		log.info("Company has employees: " + Company.findAllCompanies().get(0).getEmployees().size());
	}
	
}
