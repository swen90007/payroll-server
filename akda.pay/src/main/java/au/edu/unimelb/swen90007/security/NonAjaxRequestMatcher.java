package au.edu.unimelb.swen90007.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.RequestMatcher;

import au.edu.unimelb.swen90007.utils.HttpServletRequestUtils;

/**
 * Checks if request is NOT an AJAX call.
 */
public class NonAjaxRequestMatcher implements RequestMatcher {
  /**
   * Matches if request is NOT an AJAX call.
   * 
   * @param httpServletRequest HTTP request
   * @return true if not AJAX request
   */
  @Override
  public boolean matches(HttpServletRequest httpServletRequest) {
    return !HttpServletRequestUtils.isAjaxRequest(httpServletRequest);
  }
}
