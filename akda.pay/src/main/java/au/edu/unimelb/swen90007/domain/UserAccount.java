package au.edu.unimelb.swen90007.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUserAccountsByUsernameEquals" })
@RooJson
public class UserAccount {

	@NotNull
	@Column(unique = true)
	private String username;

	@NotNull
	private String password;

	/**
	 * The full name of the user account. Rather than specifying a first and
	 * last name, we keep this as generic as possible to allow for middle names,
	 * honourifics, connectives etc.
	 */
	@NotNull
	private String name;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<UserRole> roles = new HashSet<UserRole>();

	public boolean hasRole(Class<? extends UserRole> roleClass) {
		boolean hasRole = false;
		for (UserRole role : this.roles) {
			if (roleClass.isInstance(role)) {
				return true;
			}
		}
		return hasRole;
	}

	@Transactional
	public String toJson() {
		// TODO(aramk) keep getting serialisation errors when trying to
		// serialise company
//		JSONSerializer serializer = new JSONSerializer().exclude("*.class", "*.password",
//				"roles.company", "roles.user");
		JSONSerializer serializer = new JSONSerializer().include("*.id", "*.username", "name").exclude("*");
		return serializer.deepSerialize(this);
	}

	public String toJson(String[] fields) {
		return new JSONSerializer().include(fields).exclude("*.class").serialize(this);
	}

	public static UserAccount fromJsonToUserAccount(String json) {
		return new JSONDeserializer<UserAccount>().use(null, UserAccount.class).deserialize(json);
	}

	public static String toJsonArray(Collection<UserAccount> collection) {
		JSONSerializer serializer = new JSONSerializer().exclude("*.class", "*.password");
		return serializer.serialize(collection);
	}

	public static String toJsonArray(Collection<UserAccount> collection, String[] fields) {
		return new JSONSerializer().include(fields).exclude("*.class").serialize(collection);
	}

}
