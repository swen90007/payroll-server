package au.edu.unimelb.swen90007.domain;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import au.edu.unimelb.swen90007.transformer.EntitySerializer;
import flexjson.JSONSerializer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public abstract class TimeSheetEvent {
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime startTime;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime endTime;
	
	private String description;
	
	private boolean approved = false;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private TimeSheet timesheet;
	
	public double getMillisDuration() {
		return endTime.getMillis() - startTime.getMillis();
	}

	public String getType() {
		return this.getClass().getSimpleName();
	}
	
	@PreRemove
	@Transactional
    public void preRemove() {
        this.setTimesheet(null);
        this.persist();
    }
	
	public String toJson() {
		JSONSerializer serializer = new EntitySerializer().include("*.id", "events", "*.description", "*.startTime", "*.endTime", "*.type").exclude("*");
        return serializer.serialize(this);
    }
	
}
