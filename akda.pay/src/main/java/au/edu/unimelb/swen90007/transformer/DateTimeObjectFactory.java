package au.edu.unimelb.swen90007.transformer;

import java.lang.reflect.Type;

import org.joda.time.DateTime;

import flexjson.ObjectBinder;
import flexjson.ObjectFactory;
import flexjson.transformer.AbstractTransformer;

public class DateTimeObjectFactory extends AbstractTransformer implements ObjectFactory {

  public void transform(Object value) {
    if (value instanceof DateTime) {
      DateTime date = (DateTime) value;
      getContext().writeQuoted(date.toString());
    } else {
      getContext().writeQuoted("");
    }
  }

  @SuppressWarnings("rawtypes")
  public Object instantiate(ObjectBinder context, Object value, Type targetType, Class targetClass) {
    return DateTime.parse(value.toString());
  }

}
