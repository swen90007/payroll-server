package au.edu.unimelb.swen90007.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

public class ControllerUtils {

	public static ResponseEntity<String> buildResponse(String body, HttpHeaders headers,
			HttpStatus status) {
		return new ResponseEntity<String>(body, headers, status);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public static ResponseEntity<String> buildJsonResponse(String json, HttpStatus status) {
		// Basic JSON headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Access-Control-Allow-Origin", "*");
		return buildResponse(json, headers, status);
	}

	public static String buildFailureMessage(String action, String entityType) {
		return "{\"error\": \"Failed to " + action + " " + entityType + "\"}";
	}

	public static String buildFailureMessage(String message) {
		return "{\"error\": " + message + "\"}";
	}

	public static ResponseEntity<String> buildSuccessResponse() {
		return buildJsonResponse("", HttpStatus.OK);
	}

	public static ResponseEntity<String> buildFailureResponse(String action, String entityType) {
		return buildJsonResponse(buildFailureMessage(action, entityType), HttpStatus.BAD_REQUEST);
	}

	public static ResponseEntity<String> buildFailureResponse(String msg) {
		return buildJsonResponse(buildFailureMessage(msg), HttpStatus.BAD_REQUEST);
	}
	
	public static ResponseEntity<String> buildNotFoundResponse(String msg) {
		return buildJsonResponse(buildFailureMessage(msg), HttpStatus.NOT_FOUND);
	}
	
	public static ResponseEntity<String> buildNotFoundResponse() {
		return buildJsonResponse(null, HttpStatus.NOT_FOUND);
	}

}
