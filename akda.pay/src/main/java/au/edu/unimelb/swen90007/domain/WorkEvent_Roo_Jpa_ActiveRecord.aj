// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package au.edu.unimelb.swen90007.domain;

import au.edu.unimelb.swen90007.domain.WorkEvent;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

privileged aspect WorkEvent_Roo_Jpa_ActiveRecord {
    
    public static long WorkEvent.countWorkEvents() {
        return entityManager().createQuery("SELECT COUNT(o) FROM WorkEvent o", Long.class).getSingleResult();
    }
    
    public static List<WorkEvent> WorkEvent.findAllWorkEvents() {
        return entityManager().createQuery("SELECT o FROM WorkEvent o", WorkEvent.class).getResultList();
    }
    
    public static WorkEvent WorkEvent.findWorkEvent(Long id) {
        if (id == null) return null;
        return entityManager().find(WorkEvent.class, id);
    }
    
    public static List<WorkEvent> WorkEvent.findWorkEventEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM WorkEvent o", WorkEvent.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public WorkEvent WorkEvent.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        WorkEvent merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
