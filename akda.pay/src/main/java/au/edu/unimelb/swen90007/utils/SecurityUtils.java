package au.edu.unimelb.swen90007.utils;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import au.edu.unimelb.swen90007.domain.UserAccount;
import au.edu.unimelb.swen90007.security.UserAccountDetails;

/**
 * Provides authorization convenience methods for Spring Security. 
 */
public class SecurityUtils {
	
	public static UserAccount getCurrentUser() {
		UserAccountDetails details = (UserAccountDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		return details.getUserAccount();
	}

	/**
	 * @param roleName
	 *            A {#link GrantedAuthority} name.
	 * @return Whether the current user has the given role name.
	 */
	public static boolean userHasRole(String roleName) {
		UserAccountDetails details = (UserAccountDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		for (GrantedAuthority auth : details.getAuthorities()) {
			if (auth.getAuthority().equals(roleName)) {
				return true;
			}
		}
		return false;
	}

	// TODO(aramk) add a method to return a HTTP code 403.

	/**
	 * @param roleName
	 *            A {#link GrantedAuthority} name.
	 * @throws UserRoleException
	 *             if the current user doesn't have the role.
	 */
	public static void authorizeRole(String roleName) throws UserRoleException {
		if (!userHasRole(roleName)) {
			throw new UserRoleException("User does not have role: " + roleName);
		}
	}

	/**
	 * @param roleNames
	 *            A collection of {#link GrantedAuthority} names.
	 * @throws UserRoleException
	 *             if the current user doesn't have all the roles.
	 */
	public static void authorizeAllRoles(Collection<String> roleNames)
			throws UserRoleException {
		for (String roleName : roleNames) {
			if (!userHasRole(roleName)) {
				throw new UserRoleException("User does not have role: "
						+ roleName);
			}
		}
	}

	/**
	 * @param roleNames
	 *            A collection of {#link GrantedAuthority} names.
	 * @throws UserRoleException
	 *             if the current user doesn't have any of the roles.
	 */
	public static void authorizeAnyRoles(Collection<String> roleNames)
			throws UserRoleException {
		for (String roleName : roleNames) {
			if (userHasRole(roleName)) {
				return;
			}
		}
		throw new UserRoleException("User does not have any of the roles: "
				+ StringUtils.join(roleNames.toArray(), ", "));
	}

	/**
	 * @param roleNames
	 *            A collection of {#link GrantedAuthority} names.
	 * @throws UserRoleException
	 *             if the current user doesn't have all the roles.
	 */
	public static void authorizeAllRoles(String... roleNames)
			throws UserRoleException {
		authorizeAllRoles(Arrays.asList(roleNames));
	}

	/**
	 * @param roleNames
	 *            A collection of {#link GrantedAuthority} names.
	 * @throws UserRoleException
	 *             if the current user doesn't have any of the roles.
	 */
	public static void authorizeAnyRoles(String... roleNames)
			throws UserRoleException {
		authorizeAnyRoles(Arrays.asList(roleNames));
	}

	/**
	 * Indicates that the current user does not satisfy certain role(s).
	 */
	public static class UserRoleException extends Exception {
		private static final long serialVersionUID = 1L;

		public UserRoleException(String string) {
			super(string);
		}
	}

}
