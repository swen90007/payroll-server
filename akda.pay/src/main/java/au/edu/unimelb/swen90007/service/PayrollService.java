package au.edu.unimelb.swen90007.service;


public class PayrollService {
	
	final int maxHours = 38;
	final double superRate = 0.0925;
	final double overtimeBonus = 80.00;
	final double overtimeMultiplier = 2.0;
	
	/** 
	 * Calculates the amount of overtime hours to be paid given amount of hours worked.
	 * 
	 */
	public double calculateOvertimeHours(double netHoursWorked){
		double overtimeHours = 0;
		if(netHoursWorked>maxHours){
			overtimeHours = netHoursWorked - maxHours;
		}
	 return overtimeHours;
	}
	
	/** Calculates the gross salary for a given timeframe
	 * @param hourlyWageRate Takes in the base rate from the particular employee
	 * @param hoursWorked an aggregate of hours worked
	 * @param hoursBreaked an aggregate of hours where the employee was on break
	 * @param overtimeBonus any overtime bonus awarded for employees who work overtime
	 * @return grossSalary, the basis to all other calculations
	 * @throws Exception
	 */
	public double calculateGrossSalary(double hourlyWageRate, double hoursWorked, double hoursBreaked, double overtimeBonus) throws Exception{
		double netHoursWorked = hoursWorked - hoursBreaked;
		double overtimeHoursWorked = calculateOvertimeHours(netHoursWorked);
		double grossSalary = (netHoursWorked-overtimeHoursWorked)*hourlyWageRate + (overtimeHoursWorked)*hourlyWageRate*overtimeMultiplier;
			if(overtimeHoursWorked>0){
				grossSalary+=overtimeBonus;
			}
		if(grossSalary < 0){
			throw new Exception("Gross is less than 0");
		}
		return grossSalary;
	}
	
	
	/**
	 *  Calculates the tax payable based on 2013-2014 tax rates
	 * Annualises the rates, by the second @param in order to calculate
	 * based on annual rates, then de-annualises. 
	 * @param grossSalary
	 * @param payCycle 0: Weekly, 1: Fortnightly, 2: Monthly
	 * @return Tax payable based on the grossSalary
	 * @throws Exception
	 */
	public double calculateTaxPayable(double grossSalary, int payCycle) throws Exception{
		double annualisedTax = 0;
		//annualise gross
		if(payCycle == 0){
			grossSalary*=52;
		} 
		else if(payCycle == 1){
			grossSalary*=26;
		}
		else if(payCycle == 2){
			grossSalary*=12;
		}
		
		// calculate tax payable
		if(grossSalary < 0){
				throw new Exception("Gross is less than 0");
		}
		else if(grossSalary <= 18200){
			return 0;
		}
		else if(grossSalary>18200 && grossSalary<=37000){
			annualisedTax = ((grossSalary - 18200)*.19);
		}
		else if(grossSalary>37000 && grossSalary<=80000){
			annualisedTax = (3572 + ((grossSalary - 37000)*0.325));
		}
		else if(grossSalary>80000 && grossSalary<=180000){
			annualisedTax = (17547 + ((grossSalary-80000)*0.37));
		}
		else if(grossSalary>180000){
			return (54547 + ((grossSalary-180000)*0.45));
		} 
		
		//deannualise
		if(payCycle == 0){
			return annualisedTax/=52;
		} 
		else if(payCycle == 1){
			return annualisedTax/=26;
		}
		else if(payCycle == 3){
			return annualisedTax/=12;
		}
		return annualisedTax;
	}
	
	public double calculateTaxPayable(double grossSalary) throws Exception{
		return calculateTaxPayable(grossSalary, 0);
	}
	
	/** Calcualtes the super payable based on the current super rate
	 * @param grossSalary
	 * @param superRate
	 * @return SuperPayable
	 * @throws Exception
	 */
	public double calculateSuperPayable(double grossSalary, double superRate) throws Exception{
		if(grossSalary < 0){
			throw new Exception("Gross is less than 0");
		}
		return grossSalary*superRate;
	}

	/**
	 * Takes all the above functions and returns the net payable after all tax, super effects.
	 * @param grossSalary
	 * @return netSalary
	 * @throws Exception
	 */
	public double calculateNetPayable(double grossSalary) throws Exception{
		if(grossSalary < 0){
			throw new Exception("Gross is less than 0");
		}
		return grossSalary - calculateSuperPayable(grossSalary, superRate) - calculateTaxPayable(grossSalary);
	}
	
}
