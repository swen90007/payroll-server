package au.edu.unimelb.swen90007.security;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import au.edu.unimelb.swen90007.domain.UserAccount;
import au.edu.unimelb.swen90007.domain.UserRole;

/**
 * UserDetails implementation with extra details as provided by UserAccount
 * entity.
 * 
 * Accessors are provided for key user account details for the server-end admin
 * interface. For external system use (e.g. via REST calls), we provide
 * getUserAccount for the web controllers (accessing via the security principal)
 * wanting information of the current user.
 * 
 * @see {@link au.edu.unimelb.civenv.hpvat.mutopia.server.UserAccount}
 */
public class UserAccountDetails implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(this.getClass());

	UserAccount userAccount;
	// TODO(aramk) rename this
	LinkedList<SystemGrantedAuthority> grantedAuthorities;

	public UserAccountDetails(UserAccount userAccount) {
		this.userAccount = userAccount;
		this.grantedAuthorities = new LinkedList<SystemGrantedAuthority>();
		for (UserRole role : userAccount.getRoles()) {
			this.grantedAuthorities.add(new SystemGrantedAuthority(role));
		}
	}
	
	@Transactional
	private static Collection<UserRole> getUserRoles(UserAccount user) {
		return user.getRoles();
	}

	/**
	 * Explicitly for JSON requests
	 * 
	 * @return UserAccount entity
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return new ArrayList<GrantedAuthority>();
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return userAccount.getPassword();
	}

	@Override
	public String getUsername() {
		return userAccount.getUsername();
	}

	public class SystemGrantedAuthority implements GrantedAuthority {
		private static final long serialVersionUID = 1L;

		String authority;

		public SystemGrantedAuthority(UserRole role) {
			this.authority = role.getClass().getSimpleName();
			logger.info("Authority created: " + this.authority);
		}

		@Override
		public String getAuthority() {
			return authority;
		}
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
