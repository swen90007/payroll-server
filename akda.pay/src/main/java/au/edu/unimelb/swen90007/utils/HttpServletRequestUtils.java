package au.edu.unimelb.swen90007.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * HTTP Servlet Utils
 */
public class HttpServletRequestUtils {
  public static boolean isAjaxRequest(HttpServletRequest httpServletRequest) {
    return "XmlHttpRequest".equalsIgnoreCase(httpServletRequest.getHeader("x-requested-with"));
  }
}
