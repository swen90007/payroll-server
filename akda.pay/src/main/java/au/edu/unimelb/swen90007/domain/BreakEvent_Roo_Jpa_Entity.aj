// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package au.edu.unimelb.swen90007.domain;

import au.edu.unimelb.swen90007.domain.BreakEvent;
import javax.persistence.Entity;

privileged aspect BreakEvent_Roo_Jpa_Entity {
    
    declare @type: BreakEvent: @Entity;
    
}
