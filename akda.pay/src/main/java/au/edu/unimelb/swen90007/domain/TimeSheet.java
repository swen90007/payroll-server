package au.edu.unimelb.swen90007.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson

public class TimeSheet {
	
	@OneToMany(mappedBy = "timesheet", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<TimeSheetEvent> events = new HashSet<TimeSheetEvent>();

	public double getWorkHours() {
		double hours = 0;
		for (TimeSheetEvent event : this.getEvents()) {
			double duration = event.getMillisDuration() / 1000 / 60 / 60;
			if (event instanceof WorkEvent) {
				hours += duration;
			}
		}
		return hours;
	}
	
	public double getBreakHours() {
		double hours = 0;
		for (TimeSheetEvent event : this.getEvents()) {
			double duration = event.getMillisDuration() / 1000 / 60 / 60;
			if (event instanceof BreakEvent) {
				hours += duration;
			}
		}
		return hours;
	}
	
}
