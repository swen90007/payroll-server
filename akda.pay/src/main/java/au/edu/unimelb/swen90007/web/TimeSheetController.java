package au.edu.unimelb.swen90007.web;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.unimelb.swen90007.domain.BreakEvent;
import au.edu.unimelb.swen90007.domain.EmployeeRole;
import au.edu.unimelb.swen90007.domain.TimeSheet;
import au.edu.unimelb.swen90007.domain.TimeSheetEvent;
import au.edu.unimelb.swen90007.domain.WorkEvent;
import au.edu.unimelb.swen90007.transformer.EntitySerializer;
import au.edu.unimelb.swen90007.utils.ControllerUtils;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RequestMapping("/timesheets")
@Controller
@RooWebScaffold(path = "timesheets", formBackingObject = TimeSheet.class)
@RooWebJson(jsonObject = TimeSheet.class)
public class TimeSheetController {
	
	private static final JSONSerializer serializer = new EntitySerializer().include("*.id", "events", "*.description", "*.startTime", "*.endTime", "*.type").exclude("*");
	private static final JSONDeserializer<HashMap<String, Object>> mapDeserializer = new JSONDeserializer<HashMap<String, Object>>();
	
	@RequestMapping(value = "employee/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    @Transactional
    public ResponseEntity<String> showEmployeeTimeSheetJson(@PathVariable("id") Long id) {
        EmployeeRole employee = EmployeeRole.findEmployeeRole(id);
        if (employee == null) {
        	return ControllerUtils.buildNotFoundResponse();
        }
        return ControllerUtils.buildJsonResponse(serializer.deepSerialize(employee.getTimesheet()), HttpStatus.OK);
    }
	
	@RequestMapping(value = "employee/{id}", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    @Transactional
    public ResponseEntity<String> createEmployeeTimeSheetEventJson(@PathVariable("id") Long id, @RequestBody String json) {
        EmployeeRole employee = EmployeeRole.findEmployeeRole(id);
        HashMap<String, Object> map = mapDeserializer.deserialize(json);
        if (employee == null) {
        	return ControllerUtils.buildNotFoundResponse();
        } else {
        	TimeSheet timesheet = employee.getTimesheet();
        	if (timesheet == null) {
        		timesheet = new TimeSheet();
        		employee.persist();
        	}
        	TimeSheetEvent event;
        	if (map.get("type").equals("BreakEvent")) {
        		event = new BreakEvent();
        	} else {
        		event = new WorkEvent();
        	}
        	event.setDescription(map.get("description").toString());
        	event.setStartTime(new DateTime(map.get("startTime").toString()));
        	event.setEndTime(new DateTime(map.get("endTime").toString()));
        	event.setTimesheet(timesheet);
        	event.persist();
        	return ControllerUtils.buildJsonResponse(serializer.deepSerialize(event), HttpStatus.OK);        	
        }
    }

}
