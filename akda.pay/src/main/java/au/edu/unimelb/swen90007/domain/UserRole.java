package au.edu.unimelb.swen90007.domain;
import javax.persistence.ManyToOne;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public abstract class UserRole {
	
	@ManyToOne()
    private UserAccount user;
	
	public String getName() {
		return this.getClass().getSimpleName().replaceAll("Role$", "");
	}
    
}
