package au.edu.unimelb.swen90007.domain;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class EmployeeRole extends UserRole {

	@NotNull
	public Double hourlyWageRate;
	
	@NotNull
	public String taxFileNumber;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Company company;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private TimeSheet timesheet;
	
    public void setTaxFileNumber(int taxFileNumber) {
    	this.setTaxFileNumber(Integer.toString(taxFileNumber));
    }
    
    public void setHourlyWageRate(int hourlyWageRate) {
        this.setHourlyWageRate(Double.valueOf(hourlyWageRate));
    }
	
	public static boolean hasRole(UserAccount user, Company company) {
		for (UserRole role : user.getRoles()) {
			if (role instanceof EmployeeRole) {
				EmployeeRole empRole = (EmployeeRole) role;
				if (empRole.getCompany() == company) {
					return true;
				}
			}
		}
		return false;
	}
	
}
