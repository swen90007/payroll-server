package au.edu.unimelb.swen90007.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import au.edu.unimelb.swen90007.utils.HttpServletRequestUtils;

/**
 * Ajax-enabled logout success handler
 */
public class AjaxEnabledLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
  /**
   * If request is AJAX, returns JSON indicating login success
   * 
   * @param request HTTP request
   * @param response HTTP response
   * @param authentication authentication object associated with login input
   * @throws IOException
   * @throws ServletException
   */
  @Override
  public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    if (HttpServletRequestUtils.isAjaxRequest(request)) {
      response.getWriter().print("{\"success\":true}");
      response.getWriter().flush();
    } else {
      super.onLogoutSuccess(request, response, authentication);
    }
  }
}
