package au.edu.unimelb.swen90007.web;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.unimelb.swen90007.domain.EmployeeRole;
import au.edu.unimelb.swen90007.domain.TimeSheet;
import au.edu.unimelb.swen90007.domain.TimeSheetEvent;
import au.edu.unimelb.swen90007.service.PayrollService;
import au.edu.unimelb.swen90007.utils.ControllerUtils;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RequestMapping("/payroll")
@Controller
@RooWebScaffold(path = "payroll", formBackingObject = TimeSheetEvent.class)
@RooWebJson(jsonObject = TimeSheetEvent.class)
public class PayrollController {

	private PayrollService service;
	private static final JSONSerializer serializer = new JSONSerializer();
	private static final JSONDeserializer<HashMap<String, Object>> mapDeserializer = new JSONDeserializer<HashMap<String, Object>>();
	
	public PayrollController() {
		service = new PayrollService();
	}
	
	@RequestMapping(value = "calculate", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    @Transactional
    public ResponseEntity<String> showEmployeeTimeSheetJson(@RequestBody String json) throws Exception {
		HashMap<String, Object> map = mapDeserializer.deserialize(json);
		EmployeeRole employee = EmployeeRole.findEmployeeRole(Long.valueOf(map.get("employeeId").toString()));
		if (employee == null) {
			return ControllerUtils.buildFailureResponse("Please specify employeeId");
		} else {
			double wageRate = 0, workHours = 0, breakHours = 0;
			TimeSheet timesheet = employee.getTimesheet();
			if (timesheet != null) {
				wageRate = employee.getHourlyWageRate();
				workHours = timesheet.getWorkHours();
				breakHours = timesheet.getBreakHours();
			}
			HashMap<String, Object> output = new HashMap<String, Object>();
			double actualGross = service.calculateGrossSalary(wageRate, workHours, breakHours, 0);
			output.put("Actual Gross $", actualGross);
			output.put("Tax Payable $", service.calculateTaxPayable(actualGross));
			output.put("Super Payable $", service.calculateSuperPayable(actualGross, 0.0925));
			output.put("Net Payable $", service.calculateNetPayable(actualGross));
			output.put("Worked Hours", workHours);
			output.put("Break Hours", breakHours);
			output.put("Wage Hours", wageRate);
			return ControllerUtils.buildJsonResponse(serializer.deepSerialize(output), HttpStatus.OK);
		}
    }
	
}
