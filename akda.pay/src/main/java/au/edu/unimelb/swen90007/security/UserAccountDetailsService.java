package au.edu.unimelb.swen90007.security;

import java.security.Principal;

import javax.persistence.TypedQuery;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import au.edu.unimelb.swen90007.domain.UserAccount;


/**
 * Custom UserDetailsService for system.
 */
public class UserAccountDetailsService implements UserDetailsService {
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserAccount userAccount;
    try {
      TypedQuery<UserAccount> userAccountTypedQuery = UserAccount.findUserAccountsByUsernameEquals(username);
      userAccount = userAccountTypedQuery.getSingleResult();
      if (userAccount == null) throw new UsernameNotFoundException("Username not found.");
    } catch (Exception e){
      throw new UsernameNotFoundException("Username not found.");
    }
    return new UserAccountDetails(userAccount);
  }

  /**
   * Looks up a {@link UserAccount} corresponding to the given principle's username.
   */
  public UserAccount getUserAccount(Principal principal) {
    return ((UserAccountDetails) loadUserByUsername(principal.getName())).getUserAccount();
  }

  /**
   * Looks up the given user's system group to determine whether they are an admin.
   */
//  public boolean isAdmin(UserAccount user) {
//  	for (UserRole role : user.getRoles()) {
////		if (role instanceof AdminRole) {
////			return true;
////		}
//	}
//  	return false;
//  }

  /**
   * Looks up the given {@link Principal}'s {@link UserAccount} and {@link SystemUserGroup} to
   * determine whether they are an admin.
   * 
   * @see #isAdmin(UserAccount)
   */
//  public boolean isAdmin(Principal principal) {
//    return isAdmin(getUserAccount(principal));
//  }
}
