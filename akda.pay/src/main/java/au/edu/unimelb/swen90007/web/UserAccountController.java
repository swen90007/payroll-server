package au.edu.unimelb.swen90007.web;

import java.security.Principal;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.unimelb.swen90007.domain.UserAccount;
import au.edu.unimelb.swen90007.domain.UserRole;
import au.edu.unimelb.swen90007.security.UserAccountDetailsService;
import au.edu.unimelb.swen90007.utils.ControllerUtils;
import au.edu.unimelb.swen90007.utils.SecurityUtils;
import au.edu.unimelb.swen90007.utils.SecurityUtils.UserRoleException;
import flexjson.JSONSerializer;

@RequestMapping("/users")
@Controller
@RooWebScaffold(path = "users", formBackingObject = UserAccount.class)
@RooWebJson(jsonObject = UserAccount.class)
public class UserAccountController {
	
	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserAccountDetailsService userSvc;

	@RequestMapping(value = "/currentUserDetails", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String> jsonGetCurrentUserDetails(Principal principal) {
		try {
			UserAccount userAccount = userSvc.getUserAccount(principal);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json; charset=utf-8");
			return new ResponseEntity<String>(userAccount.toJson(), headers,
					HttpStatus.OK);
		} catch (Exception e) {
			logger.error("UserAccount JSON GET failed: See stack trace", e);
			return ControllerUtils.buildFailureResponse("getCurrentUserDetails", "useraccounts");
		}
	}
	
	@RequestMapping(value = "/{id}/roles", method = RequestMethod.GET, headers = "Accept=application/json")
	@Transactional
	public ResponseEntity<String> listRoles(@PathVariable("id") Long id) throws UserRoleException {
		SecurityUtils.authorizeAnyRoles("EmployeeRole", "AdminRole");
		UserAccount userAccount = UserAccount.findUserAccount(id);
		if (userAccount == null) {
			return ControllerUtils.buildFailureResponse("UserAccount not found", "UserAccount");
		} else {
			Set<UserRole> roles = userAccount.getRoles();
			JSONSerializer serializer = new JSONSerializer().exclude("*.class", "*.user");
			String json = serializer.serialize(roles); 
			return ControllerUtils.buildJsonResponse(json, HttpStatus.OK);
		}
	}

}
