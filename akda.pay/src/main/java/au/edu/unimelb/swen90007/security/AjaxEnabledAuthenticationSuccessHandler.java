package au.edu.unimelb.swen90007.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import au.edu.unimelb.swen90007.utils.HttpServletRequestUtils;

/**
 * Authentication success handler that can discriminate AJAX requests.
 */
public class AjaxEnabledAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
  /**
   * If request is AJAX, returns JSON format indicating login success and clears authentication
   * attributes. Otherwise delegates to parent @link{SimpleUrlAuthenticationSuccessHandler}
   * 
   * @param request HTTP request
   * @param response HTTP response
   * @param auth authentication object associated with login input
   * @throws IOException
   * @throws ServletException
   */
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication auth) throws IOException, ServletException {
    if (HttpServletRequestUtils.isAjaxRequest(request)) {
      response.getWriter().print(
          "{\"success\":true, \"targetUrl\": \"" + this.getTargetUrlParameter() + "\"}");
      response.getWriter().flush();
      clearAuthenticationAttributes(request);
    } else {
      super.onAuthenticationSuccess(request, response, auth);
    }
  }
}
