package au.edu.unimelb.swen90007.web;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import au.edu.unimelb.swen90007.domain.TimeSheetEvent;

@RequestMapping("/events")
@Controller
@RooWebScaffold(path = "events", formBackingObject = TimeSheetEvent.class)
@RooWebJson(jsonObject = TimeSheetEvent.class)
public class TimeSheetEventController {

	private static final JSONSerializer serializer = new JSONSerializer();
	private static final JSONDeserializer<HashMap<String, Object>> mapDeserializer = new JSONDeserializer<HashMap<String, Object>>();
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity<String> updateFromJson(@RequestBody String json, @PathVariable("id") Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
//        TimeSheetEvent timeSheetEvent = TimeSheetEvent.fromJsonToTimeSheetEvent(json);
        
        HashMap<String, Object> map = mapDeserializer.deserialize(json);
        TimeSheetEvent event = TimeSheetEvent.findTimeSheetEvent(id);
        event.setDescription(map.get("description").toString());
    	event.setStartTime(new DateTime(map.get("startTime").toString()));
    	event.setEndTime(new DateTime(map.get("endTime").toString()));
        
        if (event.merge() == null) {
            return new ResponseEntity<String>(headers, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(headers, HttpStatus.OK);
    }
	
}
