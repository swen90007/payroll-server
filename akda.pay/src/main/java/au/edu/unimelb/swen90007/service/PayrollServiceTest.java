package au.edu.unimelb.swen90007.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PayrollServiceTest {
	private PayrollService service;
	
	@Before
	public void setup(){
		service = new PayrollService();
	}
	
	@Test
	public void testCalculateOvertimeHours() {
		assertEquals(0, service.calculateOvertimeHours(38),0);
		assertEquals(.1, service.calculateOvertimeHours(38.1),0.1);
		assertEquals(62, service.calculateOvertimeHours(100),0);
	}

	@Test
	public void testCalculateGrossSalary() throws Exception {
		assertEquals(740,service.calculateGrossSalary(20.00, 38, 1, 80),0);
		assertEquals(760,service.calculateGrossSalary(20.00, 39, 1, 80),0);
		assertEquals(880,service.calculateGrossSalary(20.00, 40, 1, 80),0);
	}

	@Test
	public void testCalculateTaxPayable() throws Exception {
		assertEquals(0, service.calculateTaxPayable(18200),0);
		assertEquals(0.19, service.calculateTaxPayable(18201),0);
		assertEquals(19397, service.calculateTaxPayable(85000), 0);
		assertEquals(423547, service.calculateTaxPayable(1000000),0);	
	}

	@Test
	public void testCalculateSuperPayable() throws Exception {
		assertEquals(900, service.calculateSuperPayable(10000, 0.09),0);
		assertEquals(0, service.calculateSuperPayable(0, 0.09),0);
		assertEquals(2310.75, service.calculateSuperPayable(25675, 0.09),0);
	}

	@Test
	public void testCalculateNetPayable() throws Exception {
		assertEquals(55253, service.calculateNetPayable(80000),0);
		assertEquals(16562, service.calculateNetPayable(18200),0);
		assertEquals(486453, service.calculateNetPayable(1000000),0);
		


	}
	
	
}

