package au.edu.unimelb.swen90007.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.unimelb.swen90007.domain.AdminRole;
import au.edu.unimelb.swen90007.domain.Company;
import au.edu.unimelb.swen90007.domain.EmployeeRole;
import au.edu.unimelb.swen90007.domain.HRManagerRole;
import au.edu.unimelb.swen90007.domain.UserAccount;
import au.edu.unimelb.swen90007.utils.ControllerUtils;
import au.edu.unimelb.swen90007.utils.SecurityUtils;
import au.edu.unimelb.swen90007.utils.SecurityUtils.UserRoleException;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RequestMapping("/companies")
@Controller
@RooWebScaffold(path = "companies", formBackingObject = Company.class)
@RooWebJson(jsonObject = Company.class)
public class CompanyController {
	
	private static final JSONSerializer serializer = new JSONSerializer().include("*.id", "hourlyWageRate", "taxFileNumber", "*.name", "user", "*.username").exclude("*");
	private static final JSONDeserializer<HashMap<String, Object>> mapDeserializer = new JSONDeserializer<HashMap<String, Object>>();

	/**
	 * @param id the ID of the company.
	 * @return an HTTP response listing the employees in the company, or an errors.
	 * @throws UserRoleException
	 */
	@RequestMapping(value = "/{id}/employees", method = RequestMethod.GET, headers = "Accept=application/json")
	@Transactional
	public ResponseEntity<String> listEmployees(@PathVariable("id") Long id) throws UserRoleException {
		Company company = Company.findCompany(id);
		UserAccount user = SecurityUtils.getCurrentUser();
		if (user == null || company == null) {
			return ControllerUtils.buildFailureResponse("No current user and/or company found");
		} else if (EmployeeRole.hasRole(user, company) || user.hasRole(AdminRole.class)) {
			List<EmployeeRole> employees = new ArrayList<EmployeeRole>(company.getEmployees());
			String json = serializer.deepSerialize(employees);
			return ControllerUtils.buildJsonResponse(json, HttpStatus.OK);
		} else {
			return ControllerUtils.buildFailureResponse("Insufficient permissions");
		}
	}
	
	/**
	 * @param id the ID of the company.
	 * @return an HTTP response listing the employees in the company, or an errors.
	 * @throws UserRoleException
	 */
	@RequestMapping(value = "/{id}/employees", method = RequestMethod.POST, headers = "Accept=application/json")
	@Transactional
	public ResponseEntity<String> createEmployee(@PathVariable("id") Long id, @RequestBody String json) throws UserRoleException {
		Company company = Company.findCompany(id);
		UserAccount currUser = SecurityUtils.getCurrentUser();
		if (currUser == null || company == null) {
			return ControllerUtils.buildFailureResponse("No current user and/or company found");
		} else if (HRManagerRole.hasRole(currUser, company) || currUser.hasRole(AdminRole.class)) {
			HashMap<String, Object> map = mapDeserializer.deserialize(json);
			// TODO(aramk) allow creating different roles as well.
			EmployeeRole employee = new EmployeeRole();
			employee.setCompany(company);
			UserAccount user = UserAccount.findUserAccount(Long.valueOf(map.get("user").toString()));
			employee.setUser(user);
			employee.setHourlyWageRate(Double.valueOf(map.get("hourlyWageRate").toString()));
			employee.setTaxFileNumber(map.get("taxFileNumber").toString());
			if (user == null || EmployeeRole.hasRole(currUser, company)) {
				// TODO(aramk) still possible to save twice
				return ControllerUtils.buildFailureResponse("Invalid user and company combination");
			} else {
				employee.persist();
				return ControllerUtils.buildJsonResponse(serializer.serialize(employee), HttpStatus.OK);				
			}
		} else {
			return ControllerUtils.buildFailureResponse("Insufficient permissions");
		}
	}

}
